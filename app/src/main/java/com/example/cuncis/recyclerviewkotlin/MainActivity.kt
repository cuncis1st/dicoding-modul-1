package com.example.cuncis.recyclerviewkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.cuncis.recyclerviewkotlin.adapter.CustomAdapter
import com.example.cuncis.recyclerviewkotlin.model.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.list_layout.*
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

    private var items: MutableList<User> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initData()

        recyclerview.layoutManager = LinearLayoutManager(this)

        recyclerview.adapter = CustomAdapter(items, this)
    }

    private fun initData() {
        val name = resources.getStringArray(R.array.club_name)
        val desc = resources.getStringArray(R.array.club_desc)
        val image = resources.obtainTypedArray(R.array.club_image)

        items.clear()
        for (i in name.indices) {
            items.add(User(name[i],image.getResourceId(i, 0), desc[i]))
        }

        image.recycle()
    }
}
