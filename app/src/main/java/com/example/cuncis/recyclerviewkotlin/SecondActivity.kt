package com.example.cuncis.recyclerviewkotlin

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.cuncis.recyclerviewkotlin.model.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_layout.view.*
import org.jetbrains.anko.*

class SecondActivity: AppCompatActivity() {

    private var name: String = ""
    private var description: String = ""
    lateinit var textDesc: TextView
    lateinit var imgClub: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        verticalLayout {
            padding = dip(10)
            imgClub = imageView()
            imgClub.lparams{
                width = dip(100)
                height = dip(100)
                margin = dip(16)
                gravity = Gravity.CENTER
            }

            textDesc = textView()

        }

        val intent = intent
        val bundle : Bundle = intent.extras
        val gambar = bundle.getInt("image")
        Picasso.get().load(gambar).into(imgClub)

        description = intent.getStringExtra("desc")
        textDesc.text = description


    }
}