package com.example.cuncis.recyclerviewkotlin.adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.cuncis.recyclerviewkotlin.R
import com.example.cuncis.recyclerviewkotlin.SecondActivity
import com.example.cuncis.recyclerviewkotlin.model.User
import com.squareup.picasso.Picasso
import org.jetbrains.anko.startActivity

class CustomAdapter(val userList: List<User>, val context: Context): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false))


    override fun getItemCount(): Int = userList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user: User = userList[position]

        holder.bindItem(userList[position])

        holder.cardView.setOnClickListener {
            Toast.makeText(it.context, "${position+1}. " + user.name, Toast.LENGTH_SHORT).show()
            context.startActivity<SecondActivity>("image" to user.img, "desc" to user.desc)
        }

    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val textName = itemView.findViewById<TextView>(R.id.text_name)
        private val imgClub = itemView.findViewById<ImageView>(R.id.img_club)
        val cardView = itemView.findViewById<CardView>(R.id.cardView)

        fun bindItem(items: User) {
            textName.text = items.name
            items.img?.let { Picasso.get().load(it).into(imgClub) }

        }


    }

}

